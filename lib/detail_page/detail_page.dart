import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:showcaseview/showcaseview.dart';
import 'package:test_guilde/detail_page/molecules/detail_app_bar.dart';
import 'package:test_guilde/detail_page/molecules/detail_body.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  BuildContext? myContext;
  final GlobalKey _detailBack = GlobalKey();
  final GlobalKey _detailTitle = GlobalKey();
  final GlobalKey _detail1stTeam = GlobalKey();
  final GlobalKey _detail2ndTeam = GlobalKey();
  final GlobalKey _detail3rdTeam = GlobalKey();
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    WidgetsBinding.instance?.addPostFrameCallback(
      (_) => Future.delayed(const Duration(milliseconds: 200), () {
        ShowCaseWidget.of(myContext!).startShowCase([
          _detailTitle,
          _detail1stTeam,
          _detail2ndTeam,
          _detail3rdTeam,
          _detailBack,
        ]);
      }),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ShowCaseWidget(
      onStart: (index, key) {
        log('onStart Detail Page: $index, $key');
      },
      onComplete: (index, key) {},
      blurValue: 1,
      autoPlayDelay: const Duration(milliseconds: 1500),
      autoPlay: true,
      enableAutoScroll: true,
      builder: Builder(
        builder: (context) {
          myContext = context;
          return Scaffold(
            appBar: DetailAppBar(
              scrollController: _scrollController,
              keys: [
                _detailTitle,
                _detail1stTeam,
                _detail2ndTeam,
                _detail3rdTeam,
                _detailBack,
              ],
            ),
            body: DetailBody(
              scrollController: _scrollController,
              keys: [
                _detailTitle,
                _detail1stTeam,
                _detail2ndTeam,
                _detail3rdTeam,
              ],
            ),
          );
        },
      ),
    );
  }
}
