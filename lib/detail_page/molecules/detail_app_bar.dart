import 'package:flutter/material.dart';
import 'package:showcaseview/showcaseview.dart';
import 'package:test_guilde/common/default_show_case.dart';
import 'package:test_guilde/common/scroll_to_top_function.dart';

class DetailAppBar extends StatelessWidget implements PreferredSizeWidget {
  final List<GlobalKey> keys;
  final ScrollController scrollController;
  const DetailAppBar({
    Key? key,
    required this.keys,
    required this.scrollController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      leading: DefaultShowcase(
        showcaseKey: keys[4],
        description: 'Click here to go back',
        counting: '5/5',
        beforeComplete: () {
          scrollToTop(scrollController);
          ShowCaseWidget.of(context).startShowCase(keys);
          // TODO: befroe this, add go on top
        },
        child: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(55.0);
}
