import 'package:flutter/material.dart';
import 'package:test_guilde/common/default_show_case.dart';

class TeamMessages extends StatelessWidget {
  final List<GlobalKey> keys;
  const TeamMessages({
    Key? key,
    required this.keys,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DefaultShowcase(
          showcaseKey: keys[0],
          description: '1st Team Message',
          counting: '2/5',
          child: RichText(
            text: const TextSpan(
              style: TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
              children: [
                TextSpan(text: 'Hi team,\n\n'),
                TextSpan(
                  text: 'As some of you know, we’re moving to Slack for our internal team communications. Slack is a messaging app where we can talk, share files, and work together. It also connects with tools we already use, like [add your examples here], plus 900+ other apps.\n\n',
                ),
                TextSpan(
                  text: 'Why are we moving to Slack?\n\n',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
                TextSpan(
                  text: 'We want to use the best communication tools to make our lives easier and be more productive. Having everything in one place will help us work together better and faster, rather than jumping around between emails, IMs, texts and a bunch of other programs. Everything you share in Slack is automatically indexed and archived, creating a searchable archive of all our work.',
                ),
              ],
            ),
          ),
        ),
        DefaultShowcase(
          showcaseKey: keys[1],
          description: '2nd Team Message',
          counting: '3/5',
          child: RichText(
            text: const TextSpan(
              style: TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
              children: [
                TextSpan(text: 'Hi 2nd team,\n\n'),
                TextSpan(
                  text: 'As some of you know, we’re moving to Slack for our internal team communications. Slack is a messaging app where we can talk, share files, and work together. It also connects with tools we already use, like [add your examples here], plus 900+ other apps.\n\n',
                ),
                TextSpan(
                  text: 'Why are we moving to Slack?\n\n',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
                TextSpan(
                  text: 'We want to use the best communication tools to make our lives easier and be more productive. Having everything in one place will help us work together better and faster, rather than jumping around between emails, IMs, texts and a bunch of other programs. Everything you share in Slack is automatically indexed and archived, creating a searchable archive of all our work.',
                ),
              ],
            ),
          ),
        ),
        DefaultShowcase(
          showcaseKey: keys[2],
          description: '3rd Team Message',
          counting: '3/5',
          child: RichText(
            text: const TextSpan(
              style: TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
              children: [
                TextSpan(text: 'Hi 3rd team,\n\n'),
                TextSpan(
                  text: 'As some of you know, we’re moving to Slack for our internal team communications. Slack is a messaging app where we can talk, share files, and work together. It also connects with tools we already use, like [add your examples here], plus 900+ other apps.\n\n',
                ),
                TextSpan(
                  text: 'Why are we moving to Slack?\n\n',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
                TextSpan(
                  text: 'We want to use the best communication tools to make our lives easier and be more productive. Having everything in one place will help us work together better and faster, rather than jumping around between emails, IMs, texts and a bunch of other programs. Everything you share in Slack is automatically indexed and archived, creating a searchable archive of all our work.',
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
