import 'package:flutter/material.dart';
import 'package:test_guilde/common/default_show_case.dart';
import 'package:test_guilde/detail_page/molecules/team_messages.dart';

class DetailBody extends StatelessWidget {
  final List<GlobalKey> keys;
  final ScrollController scrollController;
  const DetailBody({
    Key? key,
    required this.keys,
    required this.scrollController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: ListView(
        controller: scrollController,
        children: <Widget>[
          DefaultShowcase(
            showcaseKey: keys[0],
            description: 'Email title',
            counting: '1/5',
            child: const Text(
              'Flutter Notification',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const SizedBox(height: 16),
          const Text(
            'Hi, you have new Notification from flutter group, open slack and check it out',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 16),
          TeamMessages(
            keys: keys.sublist(1),
          ),
        ],
      ),
    );
  }
}
