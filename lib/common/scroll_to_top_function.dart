import 'package:flutter/material.dart';

void scrollToTop(ScrollController controller) => controller.animateTo(
      0,
      duration: const Duration(milliseconds: 100),
      curve: Curves.linear,
    );
