import 'package:flutter/material.dart';
import 'package:showcaseview/showcaseview.dart';
import 'package:test_guilde/common/kept_show_case/kept_show_case.dart';

class DefaultShowcase extends StatelessWidget {
  final Widget child;
  final String description;
  final GlobalKey showcaseKey;
  final String counting;
  final Function()? beforeComplete;

  final String? title;
  final ShapeBorder? shapeBorder;
  final bool? disposeOnTap;
  final EdgeInsets overlayPadding;
  final Color showcaseBackgroundColor;
  final Color textColor;
  final Function()? onTargetClick;

  const DefaultShowcase({
    Key? key,
    required this.child,
    required this.description,
    required this.showcaseKey,
    required this.counting,
    this.beforeComplete,
    this.title,
    this.disposeOnTap,
    this.shapeBorder,
    this.onTargetClick,
    this.textColor = Colors.black,
    this.overlayPadding = EdgeInsets.zero,
    this.showcaseBackgroundColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KeptShowcase(
      key: showcaseKey,
      title: title,
      description: description,
      overlayPadding: overlayPadding,
      shapeBorder: shapeBorder,
      showcaseBackgroundColor: showcaseBackgroundColor,
      textColor: textColor,
      onTargetClick: onTargetClick,
      disposeOnTap: disposeOnTap,
      labelOverlay: Material(
        color: Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              counting,
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.1,
            ),
            InkWell(
              onTap: () {
                ShowCaseWidget.of(context).dismiss();
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                ),
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: const Text(
                  'Skip',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      beforeComplete: beforeComplete,
      child: child,
    );
  }
}
