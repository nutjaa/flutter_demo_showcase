import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:showcaseview/showcaseview.dart';
import 'package:test_guilde/mail_page/mail_page.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter ShowCase',
      theme: ThemeData(
        primaryColor: const Color(0xffEE5366),
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: showCaseWidget(context),
      ),
    );
  }

  Widget showCaseWidget(BuildContext context) {
    return ShowCaseWidget(
      onStart: (index, key) {
        log('onStart Mail Page: $index, $key');
      },
      onComplete: (index, key) {},
      blurValue: 1,
      builder: Builder(builder: (context) => const MailPage()),
      autoPlayDelay: const Duration(milliseconds: 1500),
      autoPlay: true,
      enableAutoScroll: true,
    );
  }
}
